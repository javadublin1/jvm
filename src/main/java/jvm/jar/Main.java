package jvm.jar;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        /**
         * Wczytaj od użytkownika liczbę N (scannerem) a następnie
         * N razy wypisz na ekran komunikat "Hello World!" w oddzielnych liniach.
         *
         *
         *
         * *Modyfikacja:
         *
         * (zakładamy)
         * Niech (String[] args) zawiera 1 element którym jest liczba (czyli długość tablicy == 1).
         * Jeśli args zawiera element (czyli rzeczywiście długość == 1) to dokonaj parsowania parametru (args[0]) do liczby
         * a następnie tyle razy wypisz komunikat "Hello World"
         *
         * UWAGA ! jeśli parametr nie został podany, to załaduj parametr ze scannera (zgodnie z pierwszym poleceniem).
         */
        int ilosc;

        if (args.length == 1) {
            // ładujemy args[0] jako N
            ilosc = Integer.parseInt(args[0]);
        }else{
            // ładujemy N ze scannera
            Scanner scanner = new Scanner(System.in);
            System.out.println("Podaj ilość powtórzeń:");
            ilosc = scanner.nextInt();
        }
        
        // N razy wypisujemy "Hello World"

        for (int i = 0; i < ilosc; i++) {
            System.out.println("Hello World!");
        }
    }
}
