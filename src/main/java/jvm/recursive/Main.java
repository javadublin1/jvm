package jvm.recursive;

public class Main {

    public static void main(String[] args) {
//        System.out.println(rekurencyjnieOdworcString("tacocat", 1));
//        System.out.println(rekurencyjnieOdworcString("ala ma kota", 1));

        rekurencyjnieNaRekord(1);
    }

    private static void rekurencyjnieNaRekord(int n) {
        if (n == 0) {
            return;
        }
        if (n % 100 == 0) {
            System.out.println(n);
        }
        rekurencyjnieNaRekord(n + 1);
    }


    private static void printNMarks(int n) {
        for (int i = 0; i < n; i++) {
            System.out.print(">");
        }
    }


    public static String rekurencyjnieOdworcString(String tekst, int i) {
        if (tekst.length() == 1) {
            return tekst;
        }
        char literkaTejIteracji = tekst.charAt(0);

        printNMarks(i);
        System.out.println("Wchodzę do rekurencji: " + tekst.substring(1));
        String wynikRekurencji = rekurencyjnieOdworcString(tekst.substring(1), ++i);
        printNMarks(i);
        System.out.println("Wychodzę z rekurencji: " + wynikRekurencji);

        wynikRekurencji = wynikRekurencji + literkaTejIteracji;
        printNMarks(i);
        System.out.println("Dodaję literkę, obecnie: " + wynikRekurencji);

        return wynikRekurencji;
    }
}
