package jvm.javac;

public class NotANumberException extends NumberFormatException {
    public NotANumberException(String s) {
    super(s);
    }
}
