package jvm.javac;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        /**
         * Napisz aplikację która wczytuje w kolejnych liniach tekst od użytkownika.
         * Wczytuj i sprawdzaj czy użytkownik wpisał liczbę.
         *
         * Jeśli nie wpisał liczby rzuć exception "NotANumberException".
         * (Nie zamykaj aplikacji, przechwyć exception i wypisz komunikat)
         *
         * Jeśli wpisał liczbę wypisz uśmieszek.
         * Jeśli wpisze 42, to wypisz "The life, the universe and everything" i zamknij program.
         */
        Scanner scanner = new Scanner(System.in);

        Integer value = 0;
        do {
            String linia = scanner.nextLine();

            try {
                value = probaParsowania(linia);
                // tutaj próba parsowania liczby i w razie niepowodzenia rzucenie exception

                System.out.println(":-)");
            } catch (NotANumberException nane) {
                // wypisuje komunikat (zwróć uwagę na 'err' - spowoduje wypisanie tekstu czerwonym kolorem)
                // z wiadomością wyjątku.
                System.err.println(nane.getMessage());
            }
        } while (value != 42);

        System.out.println("The life, the universe and everything.");
    }

    private static Integer probaParsowania(String linia) {
        try {
            return Integer.parseInt(linia);
        } catch (NumberFormatException nfe) {
            throw new NotANumberException(":(");
        }
    }
}
