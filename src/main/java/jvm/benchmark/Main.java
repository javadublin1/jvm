package jvm.benchmark;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        // zasa
        List<Integer> list = new LinkedList<>();
        metodaDodajacaNElementow(list, 200000);

        long timeStart = System.currentTimeMillis();

        iterate(list);

        metodaDodajacaNElementow(list, 5000000);

        metodaDodajacaNElementowNaPoczatek(list, 5000000);

        long timeStop = System.currentTimeMillis();

        System.out.println(timeStop - timeStart);

        Scanner scanner = new Scanner(System.in);

        boolean isWorking = true;
        do {
            String komenda = scanner.nextLine();

            if (komenda.equalsIgnoreCase("gc")) {
                Runtime.getRuntime().gc();
            } else if (komenda.equalsIgnoreCase("clear")) {
                list.clear();
            } else if (komenda.equalsIgnoreCase("remove")) {
                removeNElements(list, Integer.parseInt(scanner.nextLine()));
            }

        } while (isWorking);
    }

    private static void removeNElements(List<Integer> list, int parseInt) {
        for (int i = 0; i < parseInt; i++) {
            list.remove((int)0);
        }
    }

    public static void metodaDodajacaNElementow(List<Integer> list, int n) {
        for (int i = 0; i < n; i++) {
            list.add(i);
        }
    }

    public static void metodaDodajacaNElementowNaPoczatek(List<Integer> list, int n) {
        for (int i = 0; i < n; i++) {
            list.add(0, i);
        }
    }

    public static void iterate(List<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            int x = list.get(i);
        }
    }
}
