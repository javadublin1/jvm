package jvm.javadoc;

import java.util.Optional;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        StudentDatabase database = new StudentDatabase();

        boolean isWorking = true;

        do {
            System.out.println("Komenda : [dodaj, dodajocene, quit, listuj]");
            String komenda = scanner.nextLine();

            if (komenda.equalsIgnoreCase("dodaj")) {
                System.out.println("Podaj imie:");
                String imie = scanner.nextLine();

                System.out.println("Podaj nazwisko:");
                String nazwisko = scanner.nextLine();

                database.dodajStudenta(imie, nazwisko);
            } else if (komenda.equalsIgnoreCase("dodajocene")) {
                System.out.println("Podaj ocene:");
                String ocena = scanner.nextLine();
                System.out.println("Podaj przedmiot:");
                String przedmiot = scanner.nextLine();

                Ocena ocenaObj = new Ocena(Przedmiot.valueOf(przedmiot), Integer.parseInt(ocena));

                System.out.println("Podaj nazwisko:");
                String nazwisko = scanner.nextLine();

                database.dodajOcene(ocenaObj, nazwisko);
            } else if (komenda.equalsIgnoreCase("znajdz")) {
                System.out.println("Podaj nazwisko:");
                String nazwisko = scanner.nextLine();

                Optional<Student> studentOptional = database.znajdzStudentOpt(nazwisko);
                if (studentOptional.isPresent()) {
//                    wyciągamy studenta z pudełka
                    Student student = studentOptional.get();

                    System.out.println(student);
                } else {
                    System.err.println("Nie znaleziono studenta.");
                }

            } else if (komenda.equalsIgnoreCase("znajdzexc")) {
                System.out.println("Podaj nazwisko:");
                String nazwisko = scanner.nextLine();

                try {
                    Student student = database.znajdzStudentExc(nazwisko);

                    System.out.println(student);
                } catch (StudentNotFoundException e) {
                    System.err.println(e.getMessage());
                }
            } else if (komenda.equalsIgnoreCase("listuj")) {
                database.listujStudentow();
            } else if (komenda.equalsIgnoreCase("quit")) {
                isWorking = false;
                System.out.println("Dziękuję, do widzenia!");
            }

        } while (isWorking);

    }
}
