package jvm.javadoc;

/**
 * Klasa reprezentuje pojedynczą ocenę, którą student otrzymał z przedmiotu.
 */
public class Ocena {
    // przedmiot
    private Przedmiot przedmiot;
    // wartość oceny
    private int ocena;

    /**
     * Konstruktor oceny, tworzy ocenę z danego przedmiotu.
     * @param przedmiot - przedmiot z którego jest to ocena.
     * @param ocena - wartość oceny.
     */
    public Ocena(Przedmiot przedmiot, int ocena) {
        this.przedmiot = przedmiot;
        this.ocena = ocena;
    }

    /**
     * Getter do przedmiotu.
     * @return zwraca przedmiot z którego jest to ocena.
     */
    public Przedmiot getPrzedmiot() {
        return przedmiot;
    }

    /**
     * Setter do przedmiotu.
     * @param przedmiot - ustawia w ocenie przedmiot
     */
    public void setPrzedmiot(Przedmiot przedmiot) {
        this.przedmiot = przedmiot;
    }

    /**
     * Getter do wartości oceny.
     * @return zwraca wartość oceny.
     */
    public int getOcena() {
        return ocena;
    }

    /**
     * Setter do wartości oceny.
     * @param ocena - wartość oceny na którą chcemy zmienić.
     */
    public void setOcena(int ocena) {
        this.ocena = ocena;
    }

    @Override
    public String toString() {
        return "Ocena{" +
                "przedmiot=" + przedmiot +
                ", ocena=" + ocena +
                '}';
    }
}
