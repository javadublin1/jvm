package jvm.javadoc;

/**
 * Enum zawierający wszystkie przedmioty z których student może uzyskiwać oceny.
 * <p>
 * Przedmiot jest wykorzystywany w klasie Ocena
 */
public enum Przedmiot {
    // przedmiot matematyka
    MATEMATYKA,
    // przedmiot "język Polski"
    POLSKI
}
