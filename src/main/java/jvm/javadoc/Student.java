package jvm.javadoc;

import java.util.ArrayList;
import java.util.List;

/**
 * Klasa reprezentuje obiekt studenta.
 */
public class Student {
    // Imie studenta
    private String imie;
    // nazwisko studenta
    private String nazwisko;

    private List<Ocena> list = new ArrayList<>();

    public Student(String imie, String nazwisko) {
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    /**
     * Getter do imienia.
     * @return zwraca imie studenta.
     */
    public String getImie() {
        return imie;
    }

    /**
     * Setter do imienia.
     * @param imie które chcemy ustawić studentowi
     */
    public void setImie(String imie) {
        this.imie = imie;
    }

    /**
     * Zwraca nazwisko studenta.
     * @return nazwisko studenta.
     */
    public String getNazwisko() {
        return nazwisko;
    }

    /**
     * Ustawia nazwisko.
     * @param nazwisko które chcemy ustawić.
     */
    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    /**
     * Zwraca oceny studenta.
     * @return lista ocen.
     */
    public List<Ocena> getList() {
        return list;
    }

    @Override
    public String toString() {
        return "Student{" +
                "imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", list=" + list +
                '}';
    }
}
