package jvm.javadoc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Klasa reprezntuje kontener do gromadzenia i zarządzania bazą danych studentów i ich ocen.
 */
public class StudentDatabase {
    private List<Student> studentList = new ArrayList<>();

    /**
     * Metoda tworzy i dodaje studenta do listy
     * @param imie - imie nowego studenta.
     * @param nazwisko - nazwisko nowego studenta.
     */
    public void dodajStudenta(String imie, String nazwisko) {
        studentList.add(new Student(imie, nazwisko));
    }

    /**
     * Metoda służy do dodawania studenta do bazy danych.
     *
     * @param nowaOcena - przygotowany obiekt ocena, do wstawienia w obiekcie student.
     * @param nazwisko  - nazwisko studenta któremu dodajemy ocenę.
     */
    public void dodajOcene(Ocena nowaOcena, String nazwisko) {
        int indeks = znajdzIndeksStudenta(nazwisko);
        if (indeks != -1) { // znaleźliśmy studenta
            Student szukany = studentList.get(indeks);
            szukany.getList().add(nowaOcena);
        } else {
            System.err.println("Nie udało się odnaleźć studenta.");
        }
    }

    /**
     * Metoda zwraca indeks (na liście) szukanego studenta.
     * @param nazwisko - nazwisko szukanego studenta.
     * @return indeks studenta (na liście) lub -1 jeśli nie udało się odnaleźć
     */
    private int znajdzIndeksStudenta(String nazwisko) {
        for (int i = 0; i < studentList.size(); i++) {
            if (studentList.get(i).getNazwisko().equalsIgnoreCase(nazwisko)) {
                return i; // zwróć pozycję na liście
            }
        }
        return -1; // czyli zwróć indeks który nie istnieje.
    }

    /**
     * Metoda szuka studenta o podanym nazwisku na liście.
     * @param nazwisko - nazwisko szukanego studenta.
     * @return Optional student - jeśli udało się odnaleźć to zawiera odnalezionego studenta, jesli nie #Optional.empty()
     */
    public Optional<Student> znajdzStudentOpt(String nazwisko) {
        for (Student student : studentList) {
            if (student.getNazwisko().equalsIgnoreCase(nazwisko)) {
                return Optional.of(student); // stwórz opakowanie, i wpakuj tam studenta.
            }
        }
        return Optional.empty(); // zwróć puste pudełko
    }

    /**
     * Metoda zwraca studenta, lub w razie niepowodzenia rzuca exception (StudentNotFoundException)
     * @param nazwisko - nazwisko szukanego studenta
     * @return - odnaleziony student
     * @throws StudentNotFoundException rzucany kiedy nie udaje się odnaleźć studenta.
     */
    public Student znajdzStudentExc(String nazwisko) throws StudentNotFoundException {
        for (Student student : studentList) {
            if (student.getNazwisko().equalsIgnoreCase(nazwisko)) {
                return student; // zwróć studenta
            }
        }
        throw new StudentNotFoundException("Student with name: " + nazwisko + " wasn't found.");
    }
    // extends Exception - wyjątek który musimy koniecznie przechwycić
    // extends RuntimeException - nie ma konieczności przechwytywania - z uwagi na rzadkość wystąpienia błędu


    /**
     * Metoda listuje (void, używa println do wypisania) wszystkich studentów.
     */
    public void listujStudentow() {
        studentList.forEach(System.out::println);
    }
}
